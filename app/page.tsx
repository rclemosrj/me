import { Github, Mail, Twitter, Linkedin, ExternalLink } from "lucide-react";
import Link from "next/link";
import React from "react";
import Particles from "./components/particles";

const navigation = [
	//{ name: "Serviços", href: "/servicos" },
	{ name: "Contact", href: "/contato" },
];

/*
old 
			<div className="my-16 text-center animate-fade-in">
				<h2 className="text-sm text-zinc-500 mx-8 ">
					Olá, meu nome é Rodrigo Lemos, sou Desenvolvedor Web.
				</h2>
			</div>
*/ 
	/*
	<div className="w-1/3 animate-fade-in items-center pt-8">
				<img
				src="https://gitlab.com/rclemosrj/me/-/raw/main/public/rclemos_pic.jpg"
				alt="avatar"
				width="70px"
				height="70px"
				className="h-16 w-16 rounded-full"
				/>
			</div>
			<div className="w-2/3 animate-fade-in">
				<h3 className="pt-4 pb-2 text-2xl font-bold leading-8 tracking-tight text-zinc-200">Rodrigo Campos Lemos</h3>
				<div className="text-gray-500">Desenvolvedor Web</div>
				<div className="flex space-x-3 pt-6">
					<Link href="mailto:rcamposlemos@icloud.com" target="_blank"><Mail color="grey" size={25} /></Link>
					<Link href="https://www.linkedin.com/in/rclemos/"><Linkedin color="grey" size={25} /></Link>
				</div>
				<div className="my-16 text-center animate-fade-in">
					<h2 className="text-sm text-zinc-500 mx-8 ">
						  Olá, meu nome é Rodrigo Lemos, sou Desenvolvedor Web.
					  </h2>
				</div>
			</div>
	*/


export default function Home() {
	return (
		<div className="flex flex-col items-center justify-center w-screen h-screen overflow-hidden bg-gradient-to-tl from-black via-zinc-600/20 to-black">
			<nav className="my-16 animate-fade-in">
				<ul className="flex items-center justify-center gap-4">
					{navigation.map((item) => (
						<Link
							key={item.href}
							href={item.href}
							className="text-sm duration-500 text-zinc-500 hover:text-zinc-300"
						>
							{item.name}
						</Link>
					))}
				</ul>
			</nav>
			<div className="hidden w-screen h-px animate-glow md:block animate-fade-left bg-gradient-to-r from-zinc-300/0 via-zinc-300/50 to-zinc-300/0" />
			<Particles
				className="absolute inset-0 -z-10 animate-fade-in"
				quantity={100}
			/>
			<h1 className="z-10 text-4xl text-transparent duration-1000 bg-white cursor-default text-edge-outline animate-title font-display sm:text-6xl md:text-9xl whitespace-nowrap bg-clip-text">
				RCLemos
			</h1>

			<div className="hidden w-screen h-px animate-glow md:block animate-fade-right bg-gradient-to-r from-zinc-300/0 via-zinc-300/50 to-zinc-300/0" />
			<div className="my-16 text-center animate-fade-in">
				<h2 className="text-sm text-zinc-500 mx-8 ">
				     Hey! I'm Rodrigo Lemos, a tech-savvy Web Developer and IT Coordinator
				</h2>
			</div>

	</div>
	);

}
