import "../global.css";
import { Inter } from "@next/font/google";
import LocalFont from "@next/font/local";
import { Metadata } from "next";
//import { Analytics } from "./components/analytics";
import { Analytics } from '@vercel/analytics/react';


export const metadata: Metadata = {
	title: {
		default: "Rodrigo Campos Lemos",
		template: "%s | me-rclemosrj.vercel.app",
	},
	description: "IT Support Specialist and Web Developer",
	openGraph: {
		title: "RCLemos",
		description:
			"IT Support Specialist and Web Developer",
		url: "https://rclemos.com/",
		siteName: "RCLemos",
		images: [
			{
				url: "https://rclemosrj.github.io/me/assets/img/WhatsApp%20Image%202023-05-12%20at%2003.48.24.jpeg",
				width: 775,
				height: 1024,
			},
		],
		locale: "en-US",
		type: "website",
	},
	robots: {
		index: true,
		follow: true,
		googleBot: {
			index: true,
			follow: true,
			"max-video-preview": -1,
			"max-image-preview": "large",
			"max-snippet": -1,
		},
	},
	twitter: {
		title: "¯\_(ツ)_/¯",
		card: "summary_large_image",
	},
	icons: {
		shortcut: "/favicon.png",
	},
};
const inter = Inter({
	subsets: ["latin"],
	variable: "--font-inter",
});

const calSans = LocalFont({
	src: "../public/fonts/CalSans-SemiBold.ttf",
	variable: "--font-calsans",
});

/**
 * 
 * Google Analyts Code
 */


export default function RootLayout({
	children,
}: {
	children: React.ReactNode;
}) {
	return (
		<html lang="en" className={[inter.variable, calSans.variable].join(" ")}>
			<head>
				<Analytics />
				<script
				/*src="https://beamanalytics.b-cdn.net/beam.min.js"*/
				data-token="519a9f9a-43b4-4ab2-b7f9-2d8bdeefff98"
				async
				/>
				
			</head>
			<body
				className={`bg-black ${
					process.env.NODE_ENV === "development" ? "debug-screens" : undefined
				}`}
			>
				{children}
			</body>
		</html>
	);
}
