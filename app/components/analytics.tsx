"use client";

export function Analytics() {
	const token = process.env.NEXT_PUBLIC_BEAM_TOKEN;
	if (!token) {
		return null;
	}
	return (
		<script
			src="https://beamanalytics.b-cdn.net/beam.min.js"
			data-token="519a9f9a-43b4-4ab2-b7f9-2d8bdeefff98"
			async
		/>

	);
}
